<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/site.css">
    <title>Fortune</title>
</head>
<body>
    <div id="fortune">
        <div class="main">
            <div id="wallet">
                <div id="money">
                    <h2>Ваши деньги:</h2>
                    <span>0</span>
                </div>
                <div id="items">
                    <h2>Ваши предметы:</h2>
                    <span></span>
                </div>
                <div id="bonus">
                    <h2>Ваши бонусы:</h2>
                    <span>0</span>
                </div>
            </div>
            <div id="loading">
                <img src="img/ajax-loader.gif" alt="">
            </div>
            <div id="prize">
                <span>BMW X5</span>
            </div>
            <a href="#" id="spin" class="btn btn-primary">Испытать удачу!</a>
            <div class="actions-container">
                <div id="actions">
                    <a href="#" id="accept" class="btn btn-success">Принять</a>
                    <a href="#" id="decline" class="btn btn-danger">Отказаться</a>
                </div>
            </div>
        </div>
    </div>
    <script
        src="https://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>
    <script src="js/ajax.js"></script>
</body>
</html>