updateUserWallet();
updateUserPrize();

$('#spin').click(eventHandler);

// принятие подарка
$('#accept').click(function (e) {
    var a = $('#accept').attr('value');
    e.preventDefault();
    $.ajax({
        'url': '/accept_prize.php',
        'data': {
            'id': a
        },
        'success': function (data) {
            data = JSON.parse(data);
            if (data.response == 'ok')
            {
                updateUserWallet();
                updateUserPrize();
                $('#prize span').fadeOut();
                $('#prize span').text('Отлично!').fadeIn();
                $('#spin').bind('click', eventHandler).fadeIn();
                $('.actions-container').fadeOut();
            }
        }
    });
});

// отказ от подарка
$('#decline').click(function (e) {
    e.preventDefault();
    $('#prize span').fadeOut();
    $('#prize span').text('Что ж, попробуем ещё раз?').fadeIn();
    $('#spin').bind('click', eventHandler).fadeIn();
    $('.actions-container').fadeOut();
});

function eventHandler(e) {
    e.preventDefault();
    launch();
}

//запуск колеса
function launch() {
    $.ajax({
        'url': '/give_prize.php',
        'success': function (data) {
            data = JSON.parse(data);
            if (data.error == 'no prize' || data.prize_name == 'Nothing')
            {
                $('#prize span').text('Вы ничего не выиграли :(').fadeIn();
                return false;
            }
            if (data.prize_type_id == 1 || data.prize_type_id == 2)
            {
                updateUserWallet();
            }
            $('#spin').unbind('click').fadeOut();
            $('#prize span').fadeOut();
            $('#loading > img, .actions-container').fadeIn();
            updateUserPrize();
            $('#loading > img').fadeOut();
            $('#prize span').text('Вы выиграли ' + data.prize_name + '!').fadeIn();
            $('#accept').attr('value', data.id);
        }
    });
}

// получить деньги и бонусы с кошелька
function updateUserWallet() {
    $.ajax({
        'url': '/get-wallet.php',
        'success': function (data) {
            data = JSON.parse(data);
            $('#money span').text(data.money);
            $('#bonus span').text(data.bonus);
        }
    });
}

//Получить призы предметы
function updateUserPrize() {
    $.ajax({
        'url': '/get-prizes.php',
        'success': function (data) {
            if (data != '' || data != undefined || data != null)
                $('#items span').text(data);
        }
    });
}