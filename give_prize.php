<?php

require_once 'db.php';

$prizes = $db
        ->query('SELECT * FROM prize ORDER BY prize_value')
        ->fetchAll(2);
actionGiveAPrize($prizes);

function actionGiveAPrize($prizes)
{
    // Делаем вероятность выигрыша маленькой
    $iterations = count($prizes) * 30;

    // Добавляем пустышки, которые ведут к проигрышу
    for($i = 0; $i <= $iterations; $i++)
    {
        array_push($prizes, ['prize_name' => 'Nothing', 'prize_value' => rand(0.1, 1)]);
    }

    // расставляем вероятности для призов и выбираем случайный
    $probabilities = getProbabilities($prizes, 1/4);
    $prize = getRandomPrize($probabilities);

    if (!empty($prize))
        echo json_encode($prize);
    else
        echo json_encode(['error' => 'no prize']);
}

/**
 * Вычисляет вероятность выпадения для каждого приза, основываясь на формуле.
 *
 * @param $prizes_arr Массив со всеми призами
 * @param $total_prob Коэффициент периодичности выпадения призов
 * @return array Результирующий массив призов с шансами на выпадение
 */
function getProbabilities($prizes_arr, $total_prob)
{
    $harmonic_sum = 0;
    $result_arr = [];

    /* Складываем обратные значения цен */
    foreach ($prizes_arr as $prize) {
        if ($prize['prize_value'] != 0)
        {
            $harmonic_sum += 1 / $prize['prize_value'];
        }
    }

    /* Пробегаемся по получившимся призам и вычисляем их шанс выпадения */
    foreach ($prizes_arr as $prize)
    {
        if ($prize['prize_value'] != 0)
        {
            $prize['chance'] = $total_prob / ($prize['prize_value'] * $harmonic_sum);
            array_push($result_arr, $prize);
        }
    }

    return $result_arr;
}

/**
 * Выдаёт случайный приз из массива
 *
 * @param $prizes Массив призов с шансами на выпадение, которые получены из функции выше
 * @return mixed
 */
function getRandomPrize($prizes)
{
    $randArray = array();

    // тасуем призы
    foreach ($prizes as $prize) {
        for ($i = 0; $i < $prize['chance']; $i++) {
            $randArray[] = $prize;
        }
    }

    // вытаскиваем и возвращаем случайный
    if (!empty($randArray))
        return $randArray[mt_rand(0, count($randArray) - 1)];
    else
        return [];
}