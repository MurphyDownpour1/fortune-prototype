<?php

require_once 'db.php';

$id = intval($_GET['id']);

$prize = $db
        ->query("SELECT * FROM prize WHERE id = $id")
        ->fetch(2);


//Если тип = 1, значит это деньги, 2 - бонус, 3 - предмет
//Уменьшаем количество приза на 1

if ($prize['prize_type_id'] == 1)
{
    $db
        ->query(
            "UPDATE wallet SET money = money + " . intval($prize['prize_value']) . " WHERE id = 1"
        )->execute();

    if ($prize['prize_quantity'] != 0)
        $db
            ->query("UPDATE prize SET prize_quantity = " . --$prize['prize_quantity'] . " WHERE id = " . $id)
            ->execute();
    else
        $db
            ->query("DELETE FROM prize WHERE id = " . $id)
            ->execute();
    echo json_encode(['response' => 'ok']);
}

elseif ($prize['prize_type_id'] == 2)
{
    $db
        ->query(
            "UPDATE wallet SET bonus = bonus + " . intval($prize['prize_value']) . " WHERE id = 1"
        )->execute();

    if ($prize['prize_quantity'] > 1)
        $db
            ->query("UPDATE prize SET prize_quantity = " . --$prize['prize_quantity'] . " WHERE id = " . $id)
            ->execute();
    elseif($prize['prize_quantity'] == 1)
        $db
            ->query("DELETE FROM prize WHERE id = " . $id)
            ->execute();

    echo json_encode(['response' => 'ok']);
}
// Если предмет - то добавляем его в текстовый файл
elseif ($prize['prize_type_id'] == 3)
{
    file_put_contents('items.txt', $prize['prize_name'] . ' ', FILE_APPEND);
    echo json_encode(['response' => 'ok']);
}
